from datetime import date
"""
Estimados, con respecto a esta pregunta, entendi lo siguiente, si el usuario tiene 13 años en total, gana 1 día de vacaciones por año, es decir, si tiene 13, tendra 1, si tiene 14, tendrá 2
y así sucesivamente,
"""
def feriado_progresivo(anios_anteriores,fecha_actual,fecha_calculo):

    dias = 0
    if anios_anteriores >= 10:
        anios_empleador = anios_trabajados(fecha_actual,fecha_calculo)
        if anios_empleador >= 3:
            dias = anios_empleador - 2
        return dias
    return dias


def anios_trabajados(d1, d2):
    dias =  abs(d2 - d1).days
    return int(dias/365)

anios= 10
d1 = date(2013,1,1)
d2 = date(2017,10,25)

print(feriado_progresivo(anios, d1, d2))