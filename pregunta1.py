
def indemnizacion_anio(sueldo, meses):

    anios = int(meses/12)
    meses_fraccion =  abs(meses/12) - abs(int(meses/12))
    indemnizacion = 0
    if anios > 0:
        for i in range(anios):
            indemnizacion = indemnizacion+ sueldo
        if meses_fraccion > 0.5:
            indemnizacion = indemnizacion+ sueldo*meses_fraccion
        return indemnizacion

    return indemnizacion


def anios_servicio(date1, date2):

    days = abs(date2 - date1).days
    return (days/365)


meses = 19

sueldo = 100

print( indemnizacion_anio(sueldo,meses))

